cdist-type__p0d_podman(7)
=========================

NAME
----
cdist-type__p0d_podman - Install podman_.

.. _podman: https://podman.io/

DESCRIPTION
-----------
This setup podman_ on the system.

It also install ``docker-compose`` and a ``dkr_images`` script to list images.

.. Warning::
   This podman installation conflict with any Docker installation on the system!


REQUIRED PARAMETERS
-------------------
None.


OPTIONAL PARAMETERS
-------------------
None.


BOOLEAN PARAMETERS
------------------
None.


EXAMPLES
--------

.. code-block:: sh

    __p0d_podman


SEE ALSO
--------
:strong:`cdist-type__docker`\ (7),
:strong:`cdist-type__docker_compose`\ (7),
:strong:`cdist-type__p0d_docker`\ (7)


AUTHORS
-------
Romain Dartigues <romain.dartigues@gmail.com>


COPYING
-------
Copyright \(C) 2024 Romain Dartigues. You can redistribute it
and/or modify it under the terms of the GNU General Public License as
published by the Free Software Foundation, either version 3 of the
License, or (at your option) any later version.
