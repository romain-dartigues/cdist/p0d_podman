#!/bin/sh
set -e
set -u

EXCLUDE=
k=
while getopts s:x OPT
do
	case "$OPT" in
	s)
		case "$OPTARG" in
			[0-9]*) k=$OPTARG;;
			[Ii][Dd]) k=1;;
			[Ss]|[Ss]ize) k=2h;;
			[CcDd]|[Cc]rea*|[Dd]ate) k=3;;
			[Rr]epo*|[Ii]mage*|[Nn]ame) k=4;;
			*)
				printf 'Unknown sort key: "%s"\n' "$OPTARG"
				return 64
		esac
		;;
	x) EXCLUDE=1;;
	?)
		printf 'Usage: %s [-s sort-key]\n' "$0"
		return 64
	esac
done
shift $((OPTIND - 1))

FILTER=""
for arg in "$@"
do FILTER="${FILTER}${FILTER:+|}${arg}"
done

if [ -z "${FILTER##*/*}" ]
then FILTER="$(echo "$FILTER" | sed 's#/#\\&#g')"
fi

{
	echo "#ID Size CreatedAt Repository:Tag Containers Dangling"
	podman image ls --format '{{ .ID }},{{ .VirtualSize }},{{ .CreatedAt }},{{ .Repository }}:{{ .Tag }},{{.Containers}},{{ .IsDangling }}' |
	awk -F, "${FILTER:+\$4 ${EXCLUDE:+!}~ /${FILTER}/}"'{sub(/ /, "T", $3);sub(/ .*0000 UTC/, "Z", $3);print}' |
	LC_ALL=C sort -k "${k:-2n}" |
	numfmt --field=2 --to=iec
} |
column -t
